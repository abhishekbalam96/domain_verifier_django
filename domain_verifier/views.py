from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

def index(request):
	return render(request, 'app/index.html')

def search(request):
	return render(request, 'app/search.html')


import os
import functools
from domain_verifier.db import get_db
import socket 


def cleanup(domain):
	return domain.strip().split("//")[-1].split("/")[0].split('?')[0].replace('https://','').replace('http://','').replace('ftp://','').replace('www.','')

def urlvalidate(domains):
	# to extract hostnames from URLS
	## 1. url.split("//")[-1].split("/")[0].split('?')[0] -> Removes the trailing paths
	## split() and replace() remove trailing and leading spaces and substrings respectively
	## Im doing all this to avoid using urlparse
	## Filter is to remove empty entries caused by newlines

	# domains=[ w.strip().split("//")[-1].split("/")[0].split('?')[0].replace('https://','').replace('http://','').replace('ftp://','').replace('www.','') for w in domains]
	domains=[ cleanup(w) for w in domains]
	domains = list(filter(None, domains))
	return domains

def submit():
	if request.method == 'POST':
		textbox = []
		textbox = request.form['domains']
		textbox = textbox.splitlines()
		
		try:
			file = request.files['domains_file'].read().splitlines()
			file = [x.decode() for x in file]
		except KeyError:
			file=[]

		domain_list=textbox+file
		domain_list=urlvalidate(domain_list)
		# return str(domain_list)
		
		db = get_db()
		error = None
		
		if textbox==[] and file==[]:
			error="No Input Provided!"
		else:
			error = None 

		rows=db.execute(
			'SELECT domain FROM all_domains;'
		).fetchall()

		all_domains=[''.join(ix) for ix in rows]
		exists=[x for x in domain_list if x in all_domains]
		unique=[x for x in domain_list if x not in all_domains]

		if len(exists) == 0:
			msg='All domains added to database.'
		else:
			msg='Timestamp Updated for these domains:<br>'+str(exists)
		
		# return '<p>Domains: '+str(textbox)+str(len(textbox))+'<br>Files: '+str(file)+str(len(file))+'<br> All: '+str(domain_list)+str(len(domain_list))+'<br>Exists in DB: '+str(exists)+str(len(exists))+'<br>Unique: '+str(unique)+str(len(unique))+'</p>'
		
		failed=[]
		
		for x in unique:
			if db.execute(
				'INSERT INTO all_domains VALUES (?,datetime(),NULL)', (x,)
			) is None:
				failed.append(x)
		for x in exists:
			if db.execute(
				'UPDATE all_domains SET time = datetime() WHERE domain = ?', (x,)
			) is None:
				failed.append(x)
		db.commit()
		
		if len(failed)!=0:
			error='DB Commit Failed for: '+str(failed)

		if error is None:
			return render_template('app/success.html', text=msg)
		else:
			return render_template('app/error.html', text=error, again=True)	
	else: 
		return redirect('/')

def query():
	db=get_db()
	if request.method == 'POST':
		query=cleanup(request.form['query'])
		rows=db.execute('SELECT * FROM verified_domains WHERE domain = ?',(query,)).fetchone()
		response={}
		if rows==None:
			response.update({'result': 'fail'})
			return jsonify(response)
		else:
			response.update({'result': 'success'})
			response.update({'domain': str(rows['domain'])})
			response.update({'time': str(rows['time'])})
			response.update({'ip': str(rows['ip'])})
			return jsonify(response)