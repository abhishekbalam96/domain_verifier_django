from django.apps import AppConfig


class DomainVerifierConfig(AppConfig):
    name = 'domain_verifier'
