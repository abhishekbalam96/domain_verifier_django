from django.db import models

class All(models.Model):
	domain = models.CharField(max_length=100)
	time = models.DateTimeField(auto_now=True)
	verified = models.PositiveSmallIntegerField(default=0)
	def __str__(self):
		return self.domain

class Verified(models.Model):
	domain = models.CharField(max_length=100)
	time = models.DateTimeField(auto_now=True)
	sha256 = models.TextField()
	md5 = models.TextField()
	ipv4 = models.GenericIPAddressField(protocol='IPv4')
	ipv6 = models.GenericIPAddressField(protocol='IPv6')
	pcap = models.CharField(max_length=200)
	parked = models.PositiveSmallIntegerField(default=0)
	def __str__(self):
		return self.domain