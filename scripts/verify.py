import os
from threading import Thread
import hashlib
import subprocess as sp
import time
import multiprocessing
import re
import psycopg2

start_time = time.time()

def is_valid_ipv4(ip):
	pattern = re.compile(r"""
		^
		(?:
		  (?:
			# Decimal 1-255 (no leading 0's)
			[3-9]\d?|2(?:5[0-5]|[0-4]?\d)?|1\d{0,2}
		  |
			0x0*[0-9a-f]{1,2}  # Hexadecimal 0x0 - 0xFF (possible leading 0's)
		  |
			0+[1-3]?[0-7]{0,2} # Octal 0 - 0377 (possible leading 0's)
		  )
		  (?:                  # Repeat 0-3 times, separated by a dot
			\.
			(?:
			  [3-9]\d?|2(?:5[0-5]|[0-4]?\d)?|1\d{0,2}
			|
			  0x0*[0-9a-f]{1,2}
			|
			  0+[1-3]?[0-7]{0,2}
			)
		  ){0,3}
		|
		  0x0*[0-9a-f]{1,8}    # Hexadecimal notation, 0x0 - 0xffffffff
		|
		  0+[0-3]?[0-7]{0,10}  # Octal notation, 0 - 037777777777
		|
		  # Decimal notation, 1-4294967295:
		  429496729[0-5]|42949672[0-8]\d|4294967[01]\d\d|429496[0-6]\d{3}|
		  42949[0-5]\d{4}|4294[0-8]\d{5}|429[0-3]\d{6}|42[0-8]\d{7}|
		  4[01]\d{8}|[1-3]\d{0,9}|[4-9]\d{0,8}
		)
		$
	""", re.VERBOSE | re.IGNORECASE)
	return pattern.match(ip) is not None

def get_ips(domain):
	ipv4=''
	ipv6=''
	parked=0
	try:
		command='nslookup '+domain+' | grep -v "127" | grep Address:*'
		resp=sp.check_output(command,shell=True).decode('utf-8').replace('Address:','').strip().split('\n')
		a = [x.strip() for x in resp]
		a = list(dict.fromkeys(a))
		try:
			for x in a:
				if(is_valid_ipv4(x)):
					ipv4+=x+','
				else:
					ipv6+=x+','
		except Exception as e:
			print(str(a)+'_'+'Error in nslookup: '+str(e))
			pass
		try:
			command='curl -Ls '+domain
			resp=sp.check_output(command,shell=True).decode('utf-8').strip()
			if('parked' in a) or ('sale' in a):
				parked=1
		except Exception as e:
			print('Error in Curl.'+str(e))
			pass	
	except Exception as e:
		# print('Error in nslookup: '+str(e))
		pass
	if(ipv4==''):
		ipv4='None'
	if(ipv6==''):
		ipv6='None'
	return ipv4,ipv6,parked

def db_entry(domain, sha256, md5, ipv4, ipv6, parked, conn):
	try:
		conn.execute('UPDATE all_domains SET verified=0 WHERE domain=?',(domain,)).fetchone()
		rows=conn.execute('INSERT INTO verified_domains VALUES (?, datetime(), ?, ?, ?, ?, "_", ?)',(domain, sha256, md5, ipv4, ipv6, parked,)).fetchone()
		return 1
	except Exception as e:
		print('Error for '+domain+': '+str(e))
		return 0

def verify(domains,q):
	print ('Starting Process ' + str(os.getpid()))
	a=[]
	for x in domains:
		ipv4,ipv6,parked=get_ips(x)
		if(ipv4 != None and ipv6 != None):
			sha256=hashlib.sha256(x.encode('utf-8')).hexdigest()
			md5=hashlib.md5(x.encode('utf-8')).hexdigest()
			# status=db_entry(x, sha256, md5, ipv4, ipv6, parked,conn1)
			# if (status==0):
			# 	# print('Insert Error for:'+x)
			# 	continue
			# else:
			# 	continue
			a.append({
				"domain": x,
				"ipv4": ipv4,
				"ipv6": ipv6,
				"parked": parked,
				"sha256": sha256,
				"md5": md5
				})
		else:
			# print('Skipping for '+ x)
			a.append({
				"domain": x,
				"ipv4": "None",
				"ipv6": "None",
				"parked": 0,
				"sha256": "None",
				"md5": "None"
				})
			continue
	q.put(a)
	print ('Exiting Process ' + str(os.getpid()))
		

# Running Code

cores=os.cpu_count()
thread_no=2*cores+1

print('Total Available Threads: '+str(thread_no))

try:
	conn = psycopg2.connect(host='localhost',database='domains', user='ubuntu', password='pass', port='5432')
	cur = conn.cursor()
except psycopg2.Error as e:
	print('Error: '+str(e))
	exit()

# exit()
try:
	rows = cur.execute('SELECT domain from domain_verifier_all WHERE verified = 0')
except psycopg2.Error as e:
	print('Error: '+str(e))
	exit()

domains=()
for x in cur.fetchall():
	domains+=x

cur.close()
# print(str(len(domains)))
# print(domains)
exit()
# Splitting the domains into smaller chunks and assigning a thread to each chunk.
chunks=[domains[i:i + thread_no] for i in range(0, len(domains), thread_no)]

counter=1
t=[]
for i in chunks:
	# temp=domain_verify(('Thread No: '+str(counter)),i)
	q=multiprocessing.Queue()
	temp=multiprocessing.Process(target=verify, args=(i,q))
	t.append(temp)
	counter+=1

for x in t:
	x.start()

for x in t:
	x.join()

print('Exiting Main Process')
# print(q.get())


for x in q.get():
	# status=db_entry(x['domain'],x['sha256'],x['md5'],x['ipv4'],x['ipv6'],x['parked'],conn)
	domain=x['domain']
	sha256=x['sha256']
	md5=x['md5']
	ipv4=x['ipv4']
	ipv6=x['ipv6']
	parked=x['parked']
	try:
		cur.execute('UPDATE domain_verifier_all SET verified=1 WHERE domain=?',(domain,))
		cur.execute('INSERT INTO domain_verifier_verified VALUES (?, datetime(), ?, ?, ?, ?, "_", ?)',(domain, sha256, md5, ipv4, ipv6, parked,))
	except Exception as e:
		print('Error for '+domain+': '+str(e))

try:
	conn.commit()
except Exception as e:
	print('Error for '+domain+': '+str(e))
finally:
	cur.close()
	conn.close()

print('Time taken to run: %s' % (time.time() - start_time))